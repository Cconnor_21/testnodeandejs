const express = require('express');
const router = express.Router();
const {ensureAuthenticated} = require('../config/auth');
const path = require('path');

//welcome page
router.get('/', (req, res) => res.render('welcome'));

const mysql =  require('mysql');

var connection  = require('../db/conn');


router.post('/dashboard', ensureAuthenticated, (req, res) => {
  const {title, description} = req.body;
  console.log(title);
  console.log(description);
  connection.query(`INSERT INTO todos(title, description, userId) VALUES('${title}', '${description}', ${req.user.id});`, (err,rows) => {
  if(err) throw err;
  //res.send(rows);
  res.redirect('/dashboard');
  });
});


//Dashboard
router.get('/dashboard', ensureAuthenticated, (req, res) => {
  console.log(`User Name: ${req.user.name}`);
  router.use(express.static('public'));
  connection.query(`SELECT * FROM todos WHERE userId = ${req.user.id}`, (err,rows) => {
  if(err) throw err;
  //res.send(rows);
  res.render('dashboard', {
    rows:rows,
    name: req.user.name
  });
  });
});

//delete todo by id
router.get('/delete/:id', (req, res) => {
  console.log(req.params.id);
  connection.query(`DELETE FROM todos WHERE id = ${req.params.id}`, (err,rows) => {
  if(err) throw err;
  //res.send(rows);
  res.redirect('/dashboard');
  });
});


module.exports = router;
