const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const passport = require('passport');
const mysql =  require('mysql');

router.use(express.static('public'));

var connection  = require('../db/conn');

//Login Page
router.get('/login', (req, res) => res.render('login'));

//Register Page
router.get('/register', (req, res) => res.render('register'));

//Register Handle
router.post('/register', (req, res) => {
  const {name, email, password, password2} = req.body;
  let errors = [];

  //check required fields
  if(!name || !email || !password || !password2)
  {
    errors.push({msg: 'Please fill in all fields'});
  }

  //Check passwords match
  if(password !== password2){
    errors.push({msg: 'Passwords do not match'});
  }

  //Check pass length
  if(password.length < 6){
    errors.push({msg: 'Password should be at least 6 characters'});
  }

  if(errors.length > 0){
    res.render('register', {
      errors,
      name,
      email,
      password,
      password2
    });
  }else{
    //validation passed
    console.log(email);
    connection.query(`SELECT name, email, password from users where email = '${email}'`, function(error, results, fields){
      if(error) throw error;
      if(results.length){
        //User Exists and Register page is rerendered with entered fields saved
        errors.push({msg: 'Email is already registered'});
        res.render('register', {
          errors,
          name,
          email,
          password,
          password2
        });
      }else{

        const newUser = {name:name, email:email, password:password};

        //hash password
        bcrypt.genSalt(10, (err, salt) =>
          bcrypt.hash(newUser.password, salt, (err, hash) => {
          if(err) throw err;
          //set password to hashed password
          newUser.password = hash;
          //Save User
          var sql = `INSERT INTO users (name, email, password) values('${name}', '${email}', '${newUser.password}')`;
          connection.query(sql, function(error, results, fields){
            req.flash('success_msg', `${newUser.name}, You are now registered`);
            res.redirect('/users/login');
          });
        }))
      }
    });
  }
});

//Login Handle
router.post('/login', (req, res, next) => {
  passport.authenticate('local', {
    successRedirect: '/dashboard',
    failureRedirect: '/users/login',
    failureFlash: true
  })(req, res, next);
});

//logout
router.get('/logout', (req, res) => {
  req.logout();
  req.flash('success_msg', 'You are logged out');
  res.redirect('/users/login');
});


module.exports = router;
