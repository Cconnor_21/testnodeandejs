DROP DATABASE nodelogin;

CREATE DATABASE nodelogin;

USE nodelogin;

CREATE TABLE users(
  id INT(6) AUTO_INCREMENT PRIMARY KEY,
  name varchar(30) not null,
  email varchar(30) not null,
  password varchar(200) not null
) ENGINE=INNODB;

CREATE TABLE todos(
  id INT(6) AUTO_INCREMENT PRIMARY KEY,
  title varchar(30) not null,
  description varchar(30) not null,
  userId INT,
  CONSTRAINT fk_user
  FOREIGN KEY (userId)
  REFERENCES users(id)
) ENGINE=INNODB;


SELECT * FROM users;

INSERT INTO todos(title, description, userId) VALUES("Take out garbage", "this is the first todo", 1);
